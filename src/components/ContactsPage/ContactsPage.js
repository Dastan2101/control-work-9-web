import React, {Component} from 'react';
import {connect} from "react-redux";
import {getContacts, removeContact} from "../../store/actions/actionCreactor";
import Modal from "../UI/Modal/Modal";


class MainPage extends Component {

    state = {
        modal: false,
        content: null
    };

    componentDidMount() {
        this.props.getContacts();
    }

    gettingContact = (contact) => {
        this.setState({modal: true, content: contact});

    };

    hideModal = () => {
        this.setState({modal: false})
    };


    render() {
        let contacts;
        let contactsArray;

        if (this.props.contacts) {
            contactsArray = Object.keys(this.props.contacts).map(id => {
                return {...this.props.contacts[id], id}
            });
            contacts = contactsArray.map((contact,index) => {

                return (
                    <div style={{
                        border: '3px solid grey',
                        padding: '10px',
                        margin: '5px 0',
                    }} key={index}
                         onClick={() => this.gettingContact(contact)}
                    >
                        <img src={contact.image} alt="" style={{width: '100px', height: '100px'}}/>
                        <span style={{fontWeight: 'bold', fontSize: '22px'}}>{contact.name}</span>
                        {this.state.modal ? <Modal
                            name={this.state.content.name}
                            image={this.state.content.image}
                            mobile={this.state.content.mobile}
                            email={this.state.content.email}
                            id={this.state.content.id}
                            removeContact={(id) => this.props.removeContact(id)}
                            modal={this.state.modal}
                            hide={this.hideModal}/> : null
                        }

                    </div>
                )

            })
        }
        return (
            <div style={{width: '800px', margin: '5px auto'}}>
                <h2>Contacts</h2>
                <div>
                    {contacts}
                </div>
            </div>

        )
            ;
    }
}

const mapStateToProps = state => ({
    contacts: state.contacts.contacts
});

const mapDispatchToProps = dispatch => ({
    getContacts: () => dispatch(getContacts()),
    removeContact: (id) => dispatch(removeContact(id))

});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);