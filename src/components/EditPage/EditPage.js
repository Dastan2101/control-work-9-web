import React, {Component} from 'react';
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Label, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import axios from "../../axios-contacts";

class EditPage extends Component {
    state = {
        name: '',
        mobile: '',
        email: '',
        image: ''
    };

    valueChanged = (event) => {
        event.preventDefault();
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    componentDidMount() {

        axios.get('contacts/' + this.props.match.params.id + '.json').then(response => {
            if (response.data) {
                this.setState({
                    name: response.data.name,
                    mobile: response.data.mobile,
                    email: response.data.email,
                    image: response.data.image
                });
            }
        })

    }

    saveChanged = (event) => {

        event.preventDefault();

        let contact = {
            name: this.state.name,
            mobile: this.state.mobile,
            email: this.state.email,
            image: this.state.image
        };

        //через action-creator почему то не работает поэтому изменяю здесь//какой-то прогон/

        axios.put('contacts/' + this.props.match.params.id + '.json', contact).then(() => {
            this.props.history.replace('/')
        })

    };


    render() {
        return (
            <div style={{width: '800px', margin: '50px auto', padding: '50px 0'}}>
                <h2 style={{textAlign: 'center'}}>Edit page</h2>
                <Form className="ContactsForm">
                    <FormGroup row>
                        <Label for="name" sm={2}>Name</Label>
                        <Col sm={10}>
                            <Input type="text" name="name"
                                   value={this.state.name}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="name" sm={2}>Mobile number</Label>
                        <Col sm={10}>
                            <Input type="tel" name="mobile"
                                   value={this.state.mobile}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>email</Label>
                        <Col sm={10}>
                            <Input type="email" name="email"
                                   value={this.state.email}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>Image</Label>
                        <Col sm={10}>
                            <Input type="url" name="image"
                                   value={this.state.image}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>Photo preview</Label>
                        <Col sm={10}>
                            <Card>
                                <CardBody>
                                    <img width="200px"
                                         src={this.state.image}
                                         alt=""/>
                                </CardBody>
                            </Card>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col sm={{size: 10, offset: 2}}>
                            <Button type="button" onClick={this.saveChanged}>Save</Button>
                            <Button color="secondary" style={{
                                marginLeft: '20px', height: '40px',
                                padding: '0 8px'
                            }}>
                                <NavLink to="/"
                                         tag={RouterNavLink}
                                         exact
                                         style={{color: '#fff'}}
                                >
                                    Back to contacts
                                </NavLink>
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default EditPage;