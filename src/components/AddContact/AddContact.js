import React, {Component} from 'react';
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Label, NavLink} from "reactstrap";
import {connect} from "react-redux";
import {createContact} from "../../store/actions/actionCreactor";
import {NavLink as RouterNavLink} from "react-router-dom";

class AddForm extends Component {
    state = {
        name: '',
        mobile: '',
        email: '',
        image: ''
    };

    valueChanged = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    addContact = (event) => {

        event.preventDefault();

        if (this.state.name !== '' && this.state.mobile !== '' && this.state.email !== '' && this.state.image !== '') {
            let contact = {
                name: this.state.name,
                mobile: this.state.mobile,
                email: this.state.email,
                image: this.state.image
            };

            this.props.createContact(contact);

            this.setState({...this.state, name: '', mobile: '', email: '', image: ''});
        } else {
            alert('Not be empty')
        }


    };

    render() {
        return (
            <div style={{width: '800px', margin: '50px auto', padding: '50px 0'}}>
                <h2 style={{textAlign: 'center'}}>Add new contact</h2>
                <Form className="ContactsForm" onSubmit={this.addContact}>
                    <FormGroup row>
                        <Label for="name" sm={2}>Name</Label>
                        <Col sm={10}>
                            <Input type="text" name="name"
                                   value={this.state.name}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="name" sm={2}>Mobile number</Label>
                        <Col sm={10}>
                            <Input type="tel" name="mobile"
                                   value={this.state.mobile}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>email</Label>
                        <Col sm={10}>
                            <Input type="email" name="email"
                                   value={this.state.email}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>Image</Label>
                        <Col sm={10}>
                            <Input type="url" name="image"
                                   value={this.state.image}
                                   onChange={this.valueChanged}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="category" sm={2}>Photo preview</Label>
                        <Col sm={10}>
                            <Card>
                                <CardBody>
                                    <img width="200px"
                                         src={this.state.image}
                                         alt=""/>
                                </CardBody>
                            </Card>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col sm={{size: 10, offset: 2}}>
                            <Button type="submit">Save</Button>
                            <Button color="secondary" style={{
                                marginLeft: '20px', height: '40px',
                                padding: '0 8px'
                            }}>
                                <NavLink to="/"
                                         tag={RouterNavLink}
                                         exact
                                         style={{color: '#fff'}}
                                >
                                    Back to contacts
                                </NavLink>
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createContact: (data) => dispatch(createContact(data))
});

export default connect(null, mapDispatchToProps)(AddForm);