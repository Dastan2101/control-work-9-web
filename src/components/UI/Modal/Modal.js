import React from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, NavLink} from 'reactstrap';
import {NavLink as RouterNavLink} from "react-router-dom";

class ModalExample extends React.Component {


    render() {

        return (
            <div>
                <Modal isOpen={this.props.modal} toggle={this.props.hide} className={this.props.className}>
                    <ModalHeader toggle={this.props.hide}>{this.props.name}</ModalHeader>
                    <ModalBody>
                        <img src={this.props.image} alt="" style={{width: '100px', height: '100px'}}/>
                        <p>Mobile number: {this.props.mobile}</p>
                        <p>Email: {this.props.email}</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary">
                            <NavLink to={'/' + this.props.id + '/edit'}
                                     tag={RouterNavLink}
                                     style={{color: '#fff'}}
                            >
                                Edit
                            </NavLink>
                        </Button>
                        <Button color="primary" onClick={() => this.props.removeContact(this.props.id)}>Delete</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}



export default ModalExample;