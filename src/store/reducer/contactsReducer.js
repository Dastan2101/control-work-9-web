import {INIT_CONTACT_SUCCESS, INIT_SUCCESS} from "../actions/actionTypes";

const initialState = {
    contacts: null,
    contact: null,
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {

        case INIT_SUCCESS:

            return {...state, contacts: action.data};

        case INIT_CONTACT_SUCCESS:
            return {
                ...state,
                contact: action.contact
            };

        default:
            return state
    }
};


export default dishesReducer