export const INIT_REQUEST = 'INIT_REQUEST';
export const INIT_SUCCESS = 'INIT_SUCCESS';
export const INIT_FAILURE = 'INIT_FAILURE';
export const CONTACT_ID = 'CONTACT_ID';

export const INIT_CONTACT_SUCCESS = 'INIT_CONTACT_SUCCESS ';

export const initRequest = () => ({type: INIT_REQUEST});
export const initSuccess = (data) => ({type: INIT_SUCCESS, data});
export const initFailure = (error) => ({type: INIT_FAILURE, error});

export const initContact = (contact) => ({type: INIT_CONTACT_SUCCESS, contact});

