import axios from '../../axios-contacts'
import {initContact, initFailure, initRequest, initSuccess} from "./actionTypes";

export const createContact= (data) => {
    return dispatch => {
        dispatch(initRequest());
        axios.post('contacts.json', data).then(
            error => dispatch(initFailure(error))
        );
    }
};

export const getContacts = () => {
    return dispatch=> {
        dispatch(initRequest());
        axios.get('contacts.json').then(response => {
            dispatch(initSuccess(response.data));
        },error => {
            dispatch(initFailure(error))
        })
    }
};


export const removeContact = (id) => {
    return dispatch => {
        dispatch(initRequest());
        axios.delete('contacts/' + id + '.json').then(() => {
            dispatch(getContacts())
        })
    }
};
