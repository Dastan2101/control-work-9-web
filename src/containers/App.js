import React, { Component, Fragment } from 'react';
import {Route, Switch} from "react-router-dom";
import './App.css';
import ContactsPage from "../components/ContactsPage/ContactsPage";
import AddContact from "../components/AddContact/AddContact";
import ToolBar from "../components/Navigation/ToolBar/ToolBar";
import EditPage from "../components/EditPage/EditPage";

class App extends Component {
  render() {
    return (
        <Fragment>
            <ToolBar/>
      <div className="App" style={{margin: '100px 0'}}>
          <Switch>
              <Route path="/" exact component={ContactsPage}/>
              <Route path="/add" component={AddContact}/>
              <Route path="/:id/edit" component={EditPage}/>
          </Switch>
      </div>
        </Fragment>

    );
  }
}

export default App;
